"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChainOfResponsibility = void 0;
// abstract class
var AbstractHandler = /** @class */ (function () {
    function AbstractHandler() {
    }
    AbstractHandler.prototype.use = function (h) {
        this.next = h;
        return this.next;
    };
    AbstractHandler.prototype.get = function (url, callback) {
        if (this.next) {
            return this.next.get(url, callback);
        }
    };
    return AbstractHandler;
}());
var AuthMiddleware = /** @class */ (function (_super) {
    __extends(AuthMiddleware, _super);
    function AuthMiddleware(username, password) {
        var _this = _super.call(this) || this;
        _this.isAuthenticated = false;
        if (username === "bytefer" && password === "666") {
            _this.isAuthenticated = true;
        }
        return _this;
    }
    AuthMiddleware.prototype.get = function (url, callback) {
        if (this.isAuthenticated) {
            return _super.prototype.get.call(this, url, callback);
        }
        else {
            throw new Error("Not Authorized");
        }
    };
    return AuthMiddleware;
}(AbstractHandler));
var LoggerMiddleware = /** @class */ (function (_super) {
    __extends(LoggerMiddleware, _super);
    function LoggerMiddleware() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoggerMiddleware.prototype.get = function (url, callback) {
        console.log("Request url is: ".concat(url));
        return _super.prototype.get.call(this, url, callback);
    };
    return LoggerMiddleware;
}(AbstractHandler));
var Route = /** @class */ (function (_super) {
    __extends(Route, _super);
    function Route() {
        var _this = _super.call(this) || this;
        _this.urlDataMap = {
            "/api/todos": [{ title: "Learn Design Pattern" }],
            "/api/random": function () { return Math.random(); },
        };
        return _this;
    }
    Route.prototype.get = function (url, callback) {
        _super.prototype.get.call(this, url, callback);
        if (this.urlDataMap.hasOwnProperty(url)) {
            var value = this.urlDataMap[url];
            var result = typeof value === "function" ? value() : value;
            callback(result);
        }
    };
    return Route;
}(AbstractHandler));
var AbstractRegisterHandler = /** @class */ (function () {
    function AbstractRegisterHandler() {
    }
    AbstractRegisterHandler.prototype.use = function (h) {
        this.next = h;
        return this.next;
    };
    AbstractRegisterHandler.prototype.checkUser = function (role, email) {
        if (this.next) {
            return this.next.checkUser(role, email);
        }
    };
    return AbstractRegisterHandler;
}());
var RoleCheckMiddleware = /** @class */ (function (_super) {
    __extends(RoleCheckMiddleware, _super);
    function RoleCheckMiddleware(email) {
        var _this = _super.call(this) || this;
        _this.isAdmin = false;
        if (email === "manhdat") {
            _this.isAdmin = true;
        }
        return _this;
    }
    RoleCheckMiddleware.prototype.get = function (role, email) {
        if (this.isAdmin) {
            return _super.prototype.checkUser.call(this, role, email);
        }
        else {
            throw new Error("Not Authorized");
        }
    };
    return RoleCheckMiddleware;
}(AbstractRegisterHandler));
var ChainOfResponsibility = function () {
    var route = new Route();
    route.get("/api/todos", function (data) {
        console.log(JSON.stringify({ data: data }, null, 2));
    });
    route.get("/api/random", function (data) {
        console.log(data);
    });
    route.use(new AuthMiddleware("bytefer", "6661")).use(new LoggerMiddleware());
};
exports.ChainOfResponsibility = ChainOfResponsibility;
