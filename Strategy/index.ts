// ! Bài toán
/**
 * Ta có chức năng login nhưng mà login từ nhiều nền tảng khác nhau VD: FB, Google, Apple Accounts, Email.....
 */

import { loginStrategy } from "./applyPattern";
import { applyVoucher } from "./applyPatternWithJs";

const loginWithGoogle = () => {
  console.log("login bằng google");
};

const loginWithFacebook = () => {
  console.log("login bằng facebook");
};

const loginWithApple = () => {
  console.log("login bằng Apple");
};

const loginWithEmail = () => {
  console.log("login bằng Email");
};

const loginWithGithub = () => {
  console.log("login bằng Github");
};

const withoutPattern = (typeLogin) => {
  if (typeLogin === "google") {
    loginWithGoogle();
  } else if (typeLogin === "facebook") {
    loginWithFacebook();
  } else if (typeLogin === "Apple") {
    loginWithApple();
  } else if (typeLogin === "Email") {
    loginWithEmail();
  } else if (typeLogin === "Github") {
    loginWithGithub();
  }
};

const login = (typeLogin) => {
  withoutPattern(typeLogin);
};

export const Strategy = () => {
  // ! Chưa cấu hình => quá nhiều if -- else
  login("facebook");

  // ! Cấu hình theo typescript và UML strategies
  console.log("Login google", loginStrategy("google", "123"));

  console.log("Login local", loginStrategy("local", "manhdat", "123"));

  // ! Cấu hình theo Javascript
  console.log("Giá giảm: ", applyVoucher("womenDay", 2000));
};
