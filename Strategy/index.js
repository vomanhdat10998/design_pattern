"use strict";
// ! Bài toán
/**
 * Ta có chức năng login nhưng mà login từ nhiều nền tảng khác nhau VD: FB, Google, Apple Accounts, Email.....
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Strategy = void 0;
var applyPattern_1 = require("./applyPattern");
var applyPatternWithJs_1 = require("./applyPatternWithJs");
var loginWithGoogle = function () {
    console.log("login bằng google");
};
var loginWithFacebook = function () {
    console.log("login bằng facebook");
};
var loginWithApple = function () {
    console.log("login bằng Apple");
};
var loginWithEmail = function () {
    console.log("login bằng Email");
};
var loginWithGithub = function () {
    console.log("login bằng Github");
};
var withoutPattern = function (typeLogin) {
    if (typeLogin === "google") {
        loginWithGoogle();
    }
    else if (typeLogin === "facebook") {
        loginWithFacebook();
    }
    else if (typeLogin === "Apple") {
        loginWithApple();
    }
    else if (typeLogin === "Email") {
        loginWithEmail();
    }
    else if (typeLogin === "Github") {
        loginWithGithub();
    }
};
var login = function (typeLogin) {
    withoutPattern(typeLogin);
};
var Strategy = function () {
    // ! Chưa cấu hình => quá nhiều if -- else
    login("facebook");
    // ! Cấu hình theo typescript và UML strategies
    console.log("Login google", (0, applyPattern_1.loginStrategy)("google", "123"));
    console.log("Login local", (0, applyPattern_1.loginStrategy)("local", "manhdat", "123"));
    // ! Cấu hình theo Javascript
    console.log("Giá giảm: ", (0, applyPatternWithJs_1.applyVoucher)("womenDay", 2000));
};
exports.Strategy = Strategy;
