// ! Bài toán đặt ra là mỗi kì đặt biệt Shoppee sẽ có 1 chương trình khuyến mãi khác nhau và chúng ta phải viết 1 bài toán để không phải if else quá nhiều theo tên chương trình mà vẫn đảm bảo dễ maintain

const blackFriday = (prices) => {
  return prices * 0.4;
};

const womenDay = (prices) => {
  return prices * 0.6;
};

const promotionStrategies = {
  blackFriday,
  womenDay,
};

const applyVoucher = (promotion, prices) => {
  return promotionStrategies[promotion](prices);
};

module.exports = {
  applyVoucher,
};
