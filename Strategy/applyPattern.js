"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginStrategy = void 0;
var GoogleLoginStrategy = /** @class */ (function () {
    function GoogleLoginStrategy() {
    }
    GoogleLoginStrategy.prototype.authenticate = function (args) {
        var token = args[0];
        if (token === "google") {
            return true;
        }
        else {
            return false;
        }
    };
    return GoogleLoginStrategy;
}());
var LocalLoginStrategy = /** @class */ (function () {
    function LocalLoginStrategy() {
    }
    LocalLoginStrategy.prototype.authenticate = function (args) {
        var userName = args[0], passWord = args[1];
        if (userName === "manhdat" && passWord === "123") {
            return true;
        }
        else {
            return false;
        }
    };
    return LocalLoginStrategy;
}());
/**
 * strategies : {
 *  google: GoogleLoginStrategy,
 *  local: LocalLoginStrategy,
 * }
 */
var Authenticator = /** @class */ (function () {
    function Authenticator() {
        this.strategies = {};
    }
    Authenticator.prototype.use = function (name, strategy) {
        this.strategies[name] = strategy;
    };
    Authenticator.prototype.authenticate = function (name) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (!this.strategies[name]) {
            console.error("Authentication policy has not been set!");
            return false;
        }
        return this.strategies[name].authenticate.apply(null, args);
        // authenticate.apply là cú pháp apply args của typescript
    };
    return Authenticator;
}());
var auth = new Authenticator();
auth.use("google", new GoogleLoginStrategy());
auth.use("local", new LocalLoginStrategy());
function loginStrategy(mode) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return auth.authenticate(mode, args);
}
exports.loginStrategy = loginStrategy;
