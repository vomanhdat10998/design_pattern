interface Strategy {
  authenticate(args: any[]): boolean;
}

class GoogleLoginStrategy implements Strategy {
  authenticate(args) {
    const [token] = args;

    if (token === "google") {
      return true;
    } else {
      return false;
    }
  }
}

class LocalLoginStrategy implements Strategy {
  authenticate(args) {
    const [userName, passWord] = args;

    if (userName === "manhdat" && passWord === "123") {
      return true;
    } else {
      return false;
    }
  }
}

/**
 * strategies : {
 *  google: GoogleLoginStrategy,
 *  local: LocalLoginStrategy,
 * }
 */
class Authenticator {
  strategies: Record<string, Strategy> = {};
  use(name: string, strategy: Strategy) {
    this.strategies[name] = strategy;
  }
  authenticate(name: string, ...args: any) {
    if (!this.strategies[name]) {
      console.error("Authentication policy has not been set!");
      return false;
    }

    return this.strategies[name].authenticate.apply(null, args);
    // authenticate.apply là cú pháp apply args của typescript
  }
}

const auth = new Authenticator();

auth.use("google", new GoogleLoginStrategy());
auth.use("local", new LocalLoginStrategy());

export function loginStrategy(mode: string, ...args: any) {
  return auth.authenticate(mode, args);
}
