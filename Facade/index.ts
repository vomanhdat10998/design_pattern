class DiscountFee {
  calc(price) {
    return price * 0.95;
  }
}

class ShippingFee {
  calc(price) {
    return price - 18000;
  }
}

class ShopVoucherFee {
  calc(price) {
    return price - 5000;
  }
}

class PurchaseFacadePattern {
  discount: any;
  shipping: any;
  voucher: any;

  constructor() {
    this.discount = new DiscountFee();
    this.shipping = new ShippingFee();
    this.voucher = new ShopVoucherFee();
  }

  calc(priceSale) {
    priceSale = this.discount.calc(priceSale);
    priceSale = this.shipping.calc(priceSale);
    priceSale = this.voucher.calc(priceSale);

    return priceSale;
  }
}

export const FacadePattern = () => {
  const demoPrice = new PurchaseFacadePattern();

  console.log("Pricing last::", demoPrice.calc(200000));
};
