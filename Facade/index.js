"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FacadePattern = void 0;
var DiscountFee = /** @class */ (function () {
    function DiscountFee() {
    }
    DiscountFee.prototype.calc = function (price) {
        return price * 0.95;
    };
    return DiscountFee;
}());
var ShippingFee = /** @class */ (function () {
    function ShippingFee() {
    }
    ShippingFee.prototype.calc = function (price) {
        return price - 18000;
    };
    return ShippingFee;
}());
var ShopVoucherFee = /** @class */ (function () {
    function ShopVoucherFee() {
    }
    ShopVoucherFee.prototype.calc = function (price) {
        return price - 5000;
    };
    return ShopVoucherFee;
}());
var PurchaseFacadePattern = /** @class */ (function () {
    function PurchaseFacadePattern() {
        this.discount = new DiscountFee();
        this.shipping = new ShippingFee();
        this.voucher = new ShopVoucherFee();
    }
    PurchaseFacadePattern.prototype.calc = function (priceSale) {
        priceSale = this.discount.calc(priceSale);
        priceSale = this.shipping.calc(priceSale);
        priceSale = this.voucher.calc(priceSale);
        return priceSale;
    };
    return PurchaseFacadePattern;
}());
var FacadePattern = function () {
    var demoPrice = new PurchaseFacadePattern();
    console.log("Pricing last::", demoPrice.calc(200000));
};
exports.FacadePattern = FacadePattern;
