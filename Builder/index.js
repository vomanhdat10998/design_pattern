"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.run = void 0;
var User = /** @class */ (function () {
    function User(name, job, age) {
        this.name = name;
        this.job = job;
        this.age = age;
    }
    return User;
}());
var Builder = /** @class */ (function () {
    function Builder() {
    }
    Builder.prototype.setName = function (value) {
        this.name = value;
        return this;
    };
    Builder.prototype.setJob = function (value) {
        this.job = value;
        return this;
    };
    Builder.prototype.setAge = function (value) {
        this.age = value;
        return this;
    };
    Builder.prototype.build = function () {
        return new User(this.name, this.job, this.age);
    };
    return Builder;
}());
var run = function () {
    var newUser = new Builder()
        .setName("Manh dat")
        .setJob("Software Engineer")
        .setAge(25)
        .build();
    var newUserB = new Builder()
        .setName("Hải nè")
        .setJob("Software Engineer 2")
        .setAge(28)
        .build();
    console.log("newUser is A", newUser);
    console.log("newUser is B", newUserB);
};
exports.run = run;
