class User {
  constructor(public name: string, public job: string, public age: number) {}
}

class Builder {
  public name: string;
  public job: string;
  public age: number;

  setName(value) {
    this.name = value;
    return this;
  }

  setJob(value) {
    this.job = value;
    return this;
  }

  setAge(value) {
    this.age = value;
    return this;
  }

  build() {
    return new User(this.name, this.job, this.age);
  }
}

export const run = () => {
  const newUser = new Builder()
    .setName("Manh dat")
    .setJob("Software Engineer")
    .setAge(25)
    .build();

  const newUserB = new Builder()
    .setName("Hải nè")
    .setJob("Software Engineer 2")
    .setAge(28)
    .build();

  console.log("newUser is A", newUser);
  console.log("newUser is B", newUserB);
};
