/**
 * Bài toán: Tạo 1 button khi user Click vào sẽ thực hiện hành động gửi thông tin cho analytic
 * VD: Khi người dùng click vào nút bất kì trên trang web thì analytic sẽ thực hiện hành động gửi thông tin cho phép đo lường hiệu suất của trang web
 */
export class Observation {
  private observers: any;
  static subscribe: any;
  static unSubscribe: any;
  static notify: any;

  constructor() {
    this.observers = [];
  }

  subscribe(func) {
    // ! Đẩy Function vào 1 mảng observers
    this.observers = this.observers.push(func);
  }

  unSubscribe(func) {
    // ! Remove Function ra khỏi mảng observers
    this.observers = this.observers.filter((item) => item !== func);
  }

  notify(data) {
    // ! Chạy lại tất cả các observer có trong observers
    this.observers.forEach((observer) => observer(data));
  }
}

// ! TH: User có nút button Click
const getClickButton = document.getElementById("buttonExample");

const handleClick = (data) => {
  // ! Nhận data mới rồi chạy vào notify thông báo có function khác
  console.log("only log user just onclick");
  Observation.notify(data);
};

getClickButton?.addEventListener("click", function () {
  const num = Math.random();
  handleClick(num);
});

const handleAnalytic = (data) => {
  Observation.notify(data);
  console.log("user click button and send data to analytic", data);
};

Observation?.subscribe(handleClick);
Observation?.subscribe(handleAnalytic);
