# Series Design Pattern sơ cấp

<img src="background.png" />

## Mô tả

- Đây là series những Design Pattern sơ đẳng (Basics) được biên soạn và sưu tầm bởi Mạnh Đạt.
- Code và Explain sẽ được viết bên trong folder của design pattern đó.

## Conslusion

- Strategy Pattern
- Singleton Pattern
- Builder Pattern
