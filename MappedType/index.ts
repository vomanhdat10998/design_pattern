type MyPartial<T> = {
  [P in keyof T]?: T[P];
};

type User = {
  name: string;
  passwork: string;
  address: string;
  phone: number;
};

type UserPartial = MyPartial<User>;

type Demo = {
  name?: string;
};
