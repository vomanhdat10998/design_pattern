class FancyLogs {
  constructor() {
    if (FancyLogs.instance == null) {
      this.logs = [];
      FancyLogs.instance = this;
    }

    return FancyLogs.instance;
  }

  log(mgs) {
    this.logs.push(mgs);
    console.log(`FANCY: ${mgs}`);
  }

  printTimesLogs() {
    console.log(`${this.logs.length} logs`);
  }
}

const logger = new FancyLogs();
Object.freeze(logger);

module.exports = logger;
