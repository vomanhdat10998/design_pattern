// const logger = require("./patternWithJS");
import FancyLogs from "./patternWithTS";

const logger1 = FancyLogs.getInstance();
const logger2 = FancyLogs.getInstance();

const applyPatterOnce = () => {
  console.log("Lần 1 chạy");
  logger1.log("Chạy lần 1 nè");
  logger1.printTimesLogs();
};

const applePatternTwice = () => {
  console.log("Lần 2 chạy");
  logger2.log("Chạy lần 2 nè");
  logger2.printTimesLogs();
};

function Singleton() {
  applyPatterOnce();
  applePatternTwice();
}

export default Singleton;
