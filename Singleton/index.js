"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// const logger = require("./patternWithJS");
var patternWithTS_1 = require("./patternWithTS");
var logger1 = patternWithTS_1.default.getInstance();
var logger2 = patternWithTS_1.default.getInstance();
var applyPatterOnce = function () {
    console.log("Lần 1 chạy");
    logger1.log("Chạy lần 1 nè");
    logger1.printTimesLogs();
};
var applePatternTwice = function () {
    console.log("Lần 2 chạy");
    logger2.log("Chạy lần 2 nè");
    logger2.printTimesLogs();
};
function Singleton() {
    applyPatterOnce();
    applePatternTwice();
}
exports.default = Singleton;
