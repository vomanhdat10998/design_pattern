class FancyLogs {
  private static singleton: FancyLogs;
  private logs: any[];

  private constructor() {
    this.logs = [];
  }

  public static getInstance(): FancyLogs {
    if (!FancyLogs.singleton) {
      FancyLogs.singleton = new FancyLogs();
    }

    return FancyLogs.singleton;
  }

  log(mgs) {
    this.logs.push(mgs);
    console.log(`FANCY: ${mgs}`);
  }

  printTimesLogs() {
    console.log(`${this.logs.length} logs`);
  }
}

export default FancyLogs;
