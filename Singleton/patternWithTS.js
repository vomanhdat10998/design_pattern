"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FancyLogs = /** @class */ (function () {
    function FancyLogs() {
        this.logs = [];
    }
    FancyLogs.getInstance = function () {
        if (!FancyLogs.singleton) {
            FancyLogs.singleton = new FancyLogs();
        }
        return FancyLogs.singleton;
    };
    FancyLogs.prototype.log = function (mgs) {
        this.logs.push(mgs);
        console.log("FANCY: ".concat(mgs));
    };
    FancyLogs.prototype.printTimesLogs = function () {
        console.log("".concat(this.logs.length, " logs"));
    };
    return FancyLogs;
}());
exports.default = FancyLogs;
