const notApplyPattern = () => {
  // ! Ta có một vấn đề như sau:
  // ? Một class chứa nhiều methods khác nhau

  class Shortcuts {
    openUrl(url: string) {
      console.log("Open Url Function: " + url);
    }
    shortenUrl(url: string) {
      console.log("Shortent Url Function: " + url);
    }
    sendMessage(message: string) {
      console.log("Send message Function: " + message);
    }
    translateText(text: string) {
      console.log("Translate text Function: " + text);
    }
  }

  // ? Định nghĩa 1 handler để sử dụng action theo sự kiện của user
  class UIHandler {
    constructor(public shortcut: Shortcuts) {}

    actionHandler(action: ShortcutMethods, args: string) {
      this.shortcut[action](args);
    }
  }

  type ShortcutMethods = Methods<Shortcuts>;

  type Methods<T> = {
    [P in keyof T]: T[P] extends (...args: any) => void ? P : never;
  }[keyof T];

  const shortcuts = new Shortcuts();
  const shortcutEvent = new UIHandler(shortcuts);

  shortcutEvent.actionHandler("openUrl", "abc");
  shortcutEvent.actionHandler("shortenUrl", "abc");
};

const applyPattern = () => {
  interface Command {
    name: string;
    execute(args: any): any;
  }

  class OpenUrlCommand implements Command {
    name: "OpenUrl";
    execute(args: any) {
      console.log(`this is OpenURL ${args[0]}`);
    }
  }

  class CopyUrlCommand implements Command {
    name: "CopyUrl";
    execute(args: any) {
      console.log(`this is CopyUrl ${args[0]}`);
    }
  }

  class CommandManager {
    commands: Record<string, Command> = {};

    registerCommand(name: string, command: Command) {
      this.commands[name] = command;
    }

    executeCommand(name: string | Command, ...args: any) {
      if (typeof name === "string") {
        this.commands[name].execute(args);
      } else {
        name.execute(args);
      }
    }
  }

  class UIEventHandler {
    constructor(public cmdManager: CommandManager) {}

    handleAction(name: string | Command, args: string) {
      this.cmdManager.executeCommand(name, args);
    }
  }

  const commandManager = new CommandManager();
  commandManager.registerCommand("OpenUrl", new OpenUrlCommand());
  commandManager.registerCommand("CopyUrl1", new CopyUrlCommand());

  const uiEvenHandler = new UIEventHandler(commandManager);

  uiEvenHandler.handleAction("OpenUrl", "trigger OpenURl");
  uiEvenHandler.handleAction("CopyUrl1", "trigger CopyUrl");
  uiEvenHandler.handleAction(new CopyUrlCommand(), "not direct name action");
};

export const CommandStarter = () => {
  console.log("not apply pattern");
  notApplyPattern();
  console.log("-------------------------------------------------------");
  console.log("Apply Pattern");
  applyPattern();
};
