"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandStarter = void 0;
var notApplyPattern = function () {
    // ! Ta có một vấn đề như sau:
    // ? Một class chứa nhiều methods khác nhau
    var Shortcuts = /** @class */ (function () {
        function Shortcuts() {
        }
        Shortcuts.prototype.openUrl = function (url) {
            console.log("Open Url Function: " + url);
        };
        Shortcuts.prototype.shortenUrl = function (url) {
            console.log("Shortent Url Function: " + url);
        };
        Shortcuts.prototype.sendMessage = function (message) {
            console.log("Send message Function: " + message);
        };
        Shortcuts.prototype.translateText = function (text) {
            console.log("Translate text Function: " + text);
        };
        return Shortcuts;
    }());
    // ? Định nghĩa 1 handler để sử dụng action theo sự kiện của user
    var UIHandler = /** @class */ (function () {
        function UIHandler(shortcut) {
            this.shortcut = shortcut;
        }
        UIHandler.prototype.actionHandler = function (action, args) {
            this.shortcut[action](args);
        };
        return UIHandler;
    }());
    var shortcuts = new Shortcuts();
    var shortcutEvent = new UIHandler(shortcuts);
    shortcutEvent.actionHandler("openUrl", "abc");
    shortcutEvent.actionHandler("shortenUrl", "abc");
};
var applyPattern = function () {
    var OpenUrlCommand = /** @class */ (function () {
        function OpenUrlCommand() {
        }
        OpenUrlCommand.prototype.execute = function (args) {
            console.log("this is OpenURL ".concat(args[0]));
        };
        return OpenUrlCommand;
    }());
    var CopyUrlCommand = /** @class */ (function () {
        function CopyUrlCommand() {
        }
        CopyUrlCommand.prototype.execute = function (args) {
            console.log("this is CopyUrl ".concat(args[0]));
        };
        return CopyUrlCommand;
    }());
    var CommandManager = /** @class */ (function () {
        function CommandManager() {
            this.commands = {};
        }
        CommandManager.prototype.registerCommand = function (name, command) {
            this.commands[name] = command;
        };
        CommandManager.prototype.executeCommand = function (name) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            if (typeof name === "string") {
                this.commands[name].execute(args);
            }
            else {
                name.execute(args);
            }
        };
        return CommandManager;
    }());
    var UIEventHandler = /** @class */ (function () {
        function UIEventHandler(cmdManager) {
            this.cmdManager = cmdManager;
        }
        UIEventHandler.prototype.handleAction = function (name, args) {
            this.cmdManager.executeCommand(name, args);
        };
        return UIEventHandler;
    }());
    var commandManager = new CommandManager();
    commandManager.registerCommand("OpenUrl", new OpenUrlCommand());
    commandManager.registerCommand("CopyUrl1", new CopyUrlCommand());
    var uiEvenHandler = new UIEventHandler(commandManager);
    uiEvenHandler.handleAction("OpenUrl", "trigger OpenURl");
    uiEvenHandler.handleAction("CopyUrl1", "trigger CopyUrl");
    uiEvenHandler.handleAction(new CopyUrlCommand(), "not direct name action");
};
var CommandStarter = function () {
    console.log("not apply pattern");
    notApplyPattern();
    console.log("-------------------------------------------------------");
    console.log("Apply Pattern");
    applyPattern();
};
exports.CommandStarter = CommandStarter;
