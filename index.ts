// ! Strategy Pattern
// import { Strategy } from "./Strategy";
// import Singleton from "./Singleton";
// import { run } from "./Builder";

// import { ChainOfResponsibility } from "./ChainOfResponsibility";
// import { CommandStarter } from "./Command";
import { FacadePattern } from "./Facade";

// Strategy();
// Singleton();
// ChainOfResponsibility();
// CommandStarter();

FacadePattern();
