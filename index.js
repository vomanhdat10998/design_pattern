"use strict";
// ! Strategy Pattern
// import { Strategy } from "./Strategy";
// import Singleton from "./Singleton";
// import { run } from "./Builder";
Object.defineProperty(exports, "__esModule", { value: true });
// import { ChainOfResponsibility } from "./ChainOfResponsibility";
// import { CommandStarter } from "./Command";
var Facade_1 = require("./Facade");
// Strategy();
// Singleton();
// ChainOfResponsibility();
// CommandStarter();
(0, Facade_1.FacadePattern)();
